/**
 * @Route("/proposal/clone/{id}", name="_clone_proposal"))
 * @Template()
 */
public function clone_ProposalAction(Request $request, $id)
{
    $em = $this->getManager();

    $proposal = $this->getDoctrine()
        ->getRepository('PraxisVmBundle:Proposal')
        ->findOneById($id);

    $user = $this->get('security.context')->getToken()->getUser();

    // Check if user is allowed to access that proposal
    if ($user != $proposal->getProvidedBy()) { [...] }

    // User's Institution
    $institution = $user->getInstitutionId();

    // Proposal Date Visibility Configuration to be used
    $config = $em
        ->getRepository('PraxisVmBundle:Config')
        ->find('1');

    $_date = new \DateTime();
    $_date->modify('+' . $config->getProposalVisibility() . ' month');

    // Create the form
    $form = $this->get('form.factory')->create(new ProposalType($_date));

    $form->handleRequest($request);

    // Validates the form (isValid() calls isSubmitted() by himself, so there's no need to call that method)
    if ($form->isValid()) {
        $formdata = $form->getData();

        $proposalService = new ProposalService($em);

        // Create new Proposal
        $proposal = $proposalService->createProposalFromForm($user, $institution, $formdata, $request);

        // Generate & Set Custom Id
        $customIdService = new CustomIdentifierService();
        $proposal = $customIdService->setCustomIdentifier($proposal, $em);

        // Save Proposal in DB
        $em->persist($proposal);
        $em->flush();

        // Create Email Body
        $body = $this->renderView(
            'PraxisVmBundle:Proposal:proposalAdd_email.txt.twig',
            array(
                'title' => $proposal->getTitle(),
                'user' => $user,
                'id' => $proposal->getId(),
            )
        );

        // Instantiate MailerService
        $mailerService = new MailerService();

        // Generate Email
        $message = $mailerService->createProposalSubmissionEmail($user, $body);

        // Send Email
        $this->get('mailer')->send($message);

        self::indexing();

        return $this->render('PraxisVmBundle:Proposal:add_confirm.html.twig');
    }

    //Populate Fields
    $languages = $em
        ->getRepository('PraxisVmBundle:Language')
        ->findAll();

    $studyAreas = $em
        ->getRepository('PraxisVmBundle:StudyArea')
        ->findAll();

    $vetQualifications = $em
        ->getRepository('PraxisVmBundle:VetQualification')
        ->findAll();

    $studyDegrees = $em
        ->getRepository('PraxisVmBundle:StudyDegree')
        ->findAll();

    $targets = $em
        ->getRepository('PraxisVmBundle:Target')
        ->findAll();

    $countries = $em
        ->getRepository('PraxisVmBundle:Country')
        ->findby(array(),
        array('name' => 'ASC')
        );

    $types = $em
        ->getRepository('PraxisVmBundle:InstitutionType')
        ->findAll();

    $educationalpartners = $em
        ->getRepository('PraxisVmBundle:EducationalPartner')
        ->findAll();

    return array('form' => $form->createView(), 'targets' => $targets,
        'studyDegrees' => $studyDegrees, 'studyAreas' => $studyAreas, 'vetQualifications' => $vetQualifications, 'languages' => $languages,
        'proposal' => $proposal, 'institution' => $institution, 'educationalPartners' => $educationalpartners, 'user' => $user, 'types' => $types,
        'countries' => $countries, 'anotherHost' => $anotherHost
    );

}
