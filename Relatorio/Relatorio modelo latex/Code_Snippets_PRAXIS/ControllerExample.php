<?php

namespace Praxis\VmBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use FOS\UserBundle\Util\TokenGenerator;

// these import the "@Route" and "@Template" annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;

/**
 * A Controller dedicated to Evaluation-exclusive actions
 */
class EvaluationController extends Controller {

    /**
     * @Route("/evaluateInstitution/", name="_evaluateInstitution")
     * @Template()
     */
    public function evaluateInstitutionAction(Request $request)
    {
        // Fetch from the Database everything that is needed whether the form is valid or not
        // Logged User (Provider)
        $user = $this->get('security.context')->getToken()->getUser();

        $em = $this->getManager();

        // Create the form
        $form = $this->get('form.factory')->create(new EvaluationType());

        // Processes the form data, it can now be validated
        $form->handleRequest($request);

        // Validates the form (isValid() also calls isSubmitted(), so there's no need to call that)
        if ($form->isValid()) {

            $formdata = $form->getData();

            $institutions = $request->get('institutions');
            $eps = $request->get('educationalPartners');

            // For each chosen institution and EP
            if (is_array($institutions) && is_array($eps)) {

                foreach ($eps as $ep_data) {
                    foreach ($institutions as $institution_data) {

                        // Grab the institution and EP instances from the database by id
                        $data_institution = $em->getRepository('PraxisVmBundle:Institution')->findOneById($institution_data);
                        $data_ep = $em->getRepository('PraxisVmBundle:EducationalPartner')->findOneById($ep_data);

                        // Checks if there's already an evaluation with the current pair EP-Institution
                        $evaluation = $em
                            ->createQuery('SELECT ev FROM PraxisVmBundle:Evaluation ev WHERE ev.institution = '.$data_institution->getId().' AND ev.educationalPartner = '.$data_ep->getId())
                            ->getOneOrNullResult();

                        // Creates a new instance in case the result from the query is null, otherwise, uses the existant one and updates the corresponding entry in the DB
                        if($evaluation == null) $evaluation = new Evaluation();

                        $evaluation->setInstitution($data_institution);
                        $evaluation->setEducationalPartner($data_ep);
                        $evaluation->setGrade($formdata['certification']);
                        $em->persist($evaluation);
                    }
                }

                $em->flush();

            }

            return $this->render('PraxisVmBundle:Evaluation:evaluateConfirm.html.twig');
        }

        // Get list of educational partners in which the user is PLC
        $eps = $em->createQuery('SELECT ep FROM PraxisVmBundle:EducationalPartner ep WHERE ep.PLC = '.$user->getId().' ORDER BY ep.institution ASC')
            ->getResult();

        // Get all the registered institutions
        $institutions = $em->getRepository('PraxisVmBundle:Institution')
            ->findAll();

        return array('form' => $form->createView(),
            'institutions' => $institutions,
            'educationalPartners' => $eps
        );
    }

}
