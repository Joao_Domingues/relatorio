<?php

namespace Praxis\VmBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class AssociationType extends AbstractType
{

    public function __construct()
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('institution', 'text')
        ->add('educationalPartners', 'text');
    }

    public function getName()
    {
        return 'associationRequest';
    }


    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
    }
}
