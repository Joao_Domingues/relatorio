<?php

namespace Praxis\VmBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entity that represents the identifier requested by DEI-ISEP
 * 
 * @ORM\Entity
 * @ORM\Table(name="CustomIdentifier")
 */
class CustomIdentifier
{

    // Attributes

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     */
    protected $year_id;

    /**
     * @ORM\Column(type="integer")
     */
    protected $proposal_index;

    // Constructor
    public function __construct()
    {
        $this->proposal_index = 1;
    }

    // Getters & Setters
    /**
     * Set year_id
     *
     * @param integer $year_id
     * @return CustomIdentifier
     */
    public function setYear($year_id) 
    {
        $this->year_id = $year_id;

        return $this;
    }

    /**
     * Get year_id
     *
     * @return integer 
     */
    public function getYear() 
    {
        return $this->year_id;
    }

    /**
     * Generates the custom id for the proposal
     *
     * @return integer 
     */
    public function generateIdentifier() {
        
        /* Method Logic */

        return $customId;
    }
}