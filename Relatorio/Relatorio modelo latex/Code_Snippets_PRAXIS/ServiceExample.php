<?php

namespace Praxis\VmBundle\Services;

use Praxis\VmBundle\Entity\User;

/**
 * A Service that handles the process of purging the database
 */
class PurgeService
{

    private $entityManager;

    private $users;
    [...]
    private $educationalPartners;

    public function __construct($entityManager) {
        $this->entityManager = $entityManager;
    }

    /**
     * Selects from the database all the Users that fit the filters to be purged
     *
     * @param   $plc_flag   A boolean flag that checks whether the users who PLC should (or not) be purged
     * @param   $begin_date Starting date for the filter
     * @param   $end_date   Ending date for the filter
     */
    public function getUsersToDelete($plc_flag, $begin_date, $end_date)
    {
        $users = $em->createQuery(/*Query here*/)
            ->getResult();

        if(!$plc_flag) {
            /* Remove PLC users from the to-delete list */
        }

        $this->users = $users;
        [...]
    }

    /**
     * Selects from the database all the Proposals that fit the filters to be purged
     *
     * @param   $begin_date Starting date for the filter
     * @param   $end_date   Ending date for the filter
     */
    public function getProposalsToDelete($begin_date, $end_date)
    {
        $proposals = $em->createQuery(/*Query here*/)
            ->getResult();

        [...]
    }

    /**
     * Selects from the database all the Searchs that fit the filters to be purged
     *
     * @param   $begin_date Starting date for the filter
     * @param   $end_date   Ending date for the filter
     */
    public function getSearchsToDelete($begin_date, $end_date)
    {
        $searchs = $em->createQuery(/*Query here*/)
            ->getResult();

        [...]
    }

    /**
     * Selects from the database all the DreamInternships that fit the filters to be purged
     *
     * @param   $begin_date Starting date for the filter
     * @param   $end_date   Ending date for the filter
     */
    public function getDreamInternshipsToDelete($begin_date, $end_date)
    {
        $dreamInternships = $em->createQuery(/*Query here*/)
            ->getResult();

        [...]
    }

    /**
     * Deletes the gathered information from the database
     *
     * @return $count   Total amount of entries removed, to be displayed to the user
     */
    public function deleteInformation() { [...] }

    /**
     * Generates a SQL file containing the purged information
     */
    public function generateFile() { [...] }
}
