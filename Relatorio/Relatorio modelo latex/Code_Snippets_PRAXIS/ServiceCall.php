            // Email
            // Create Email Body
            $body = $this->renderView(
                'PraxisVmBundle:Proposal:proposalAdd_email.txt.twig',
                array(
                    'title' => $proposal->getTitle(),
                    'user' => $user,
                    'id' => $proposal->getId(),
                )
            );

            // Instantiate MailerService
            $mailerService = new MailerService();

            // Generate Email
            $message = $mailerService->createProposalSubmissionEmail($user, $body);

            // Send Email
            $this->get('mailer')->send($message);
