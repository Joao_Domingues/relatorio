/**
 * Example method to showcase how Symfony deals with Forms in the Controllers
 * 
 * The methods are called twice, one returns the rendering of the Form, the second processes the information received in the Request and performs the method's main objective
 *
 * https://symfony.com/doc/current/forms.html
 */
public function exampleMethod(Request $request)
{
    // Generates a new Form instance, in this case a Proposal Form
    $form = $this->get('form.factory')->create(new ProposalType());

    // A call to handleRequest() to handle the Form Submission
    $form->handleRequest($request);

    // Verify if the Form is submitted and valid - the isSubmitted() method calls the isValid() method by himself, so the isValid() call is redundant
    if ($form->isSubmitted() && $form->isValid()) {

        // getData() holds the submitted values
        $data = $form->getData();

        // ...

        // Redirects to a success message page
        return $this->redirectToRoute('task_success');
    }

    // Renders the Form created above
    return $this->render('task/new.html.twig', [
        'form' => $form->createView(),
    ]);
}
