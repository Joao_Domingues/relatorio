<?php

namespace Praxis\VmBundle\Services;

use Praxis\VmBundle\Entity\Proposal;
use Praxis\VmBundle\Entity\User;

/**
 * A Service that handles the creation of new Emails, by generating Swift_Message instances in several different formats
 * Each method should receive the source of the receiver's email, along with the message's body
 */
class MailerService
{

    private $_defaultFrom = [...];
    private $_defaultBcc = [...];
    private $_defaultCc = [...];

    /**
     * Generates an email for Confirmation of Project/Internship Submission.
     * Uses default From & Bcc emails.
     *
     * @param   User $user The user that submitted the proposal
     * @param   string $body The body of the email
     * @return  Swift_Message The email to be sent
     */
    public function createProposalSubmissionEmail($user, $body)
    {
        $subject = 'PRAXIS Project/Internship: confirmation of Project/Internship submission';
        $to = $user->getEmail();

        $message = $this->assembleEmail($subject, $to, $body);
        $message = $this->addDefaultFrom($message);
        $message = $this->addDefaultBcc($message);

        return $message;
    }

    [...] // Other email types

    /**
     * Generates a new instance of a Swift_Message with the received information
     */
    private function assembleEmail($subject, $to, $body)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setTo($to)
            ->setBody($body)
        ;

        return $message;
    }

    /**
     * Adds the Default From value to the received email, returning it after
     */
    private function addDefaultFrom($message)
    {
        $message->setFrom($this->_defaultFrom);

        return $message;
    }

    /**
     * Adds the Default Cc value to the received email, returning it after
     */
    private function addDefaultCc($message)
    {
        $message->setCc($this->_defaultCc);

        return $message;
    }

    /**
     * Adds the Default Bcc value to the received email, returning it after
     */
    private function addDefaultBcc($message)
    {
        $message->setBcc($this->_defaultBcc);

        return $message;
    }

}
