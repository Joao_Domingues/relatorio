\babel@toc {portuguese}{}
\contentsline {chapter}{Abstract}{IX}{chapter*.2}% 
\contentsline {chapter}{Resumo}{XI}{chapter*.3}% 
\contentsline {chapter}{Lista de Figuras}{XVII}{chapter*.3}% 
\contentsline {chapter}{Lista de Tabelas}{XIX}{chapter*.3}% 
\contentsline {chapter}{Nota\IeC {\c c}\IeC {\~a}o e Gloss\IeC {\'a}rio}{XXI}{chapter*.3}% 
\contentsline {paragraph}{}{XXI}{chapter*.4}% 
\contentsline {chapter}{\numberline {1}Introdu\IeC {\c c}\IeC {\~a}o}{1}{chapter.1}% 
\contentsline {section}{\numberline {1.1}Enquadramento / Contexto}{1}{section.1.1}% 
\contentsline {section}{\numberline {1.2}Descri\IeC {\c c}\IeC {\~a}o do Problema}{2}{section.1.2}% 
\contentsline {subsection}{\numberline {1.2.1}Objetivos}{3}{subsection.1.2.1}% 
\contentsline {subsection}{\numberline {1.2.2}Abordagem}{3}{subsection.1.2.2}% 
\contentsline {subsection}{\numberline {1.2.3}Contributos}{4}{subsection.1.2.3}% 
\contentsline {subsection}{\numberline {1.2.4}Planeamento do trabalho}{5}{subsection.1.2.4}% 
\contentsline {section}{\numberline {1.3}Estrutura do relat\IeC {\'o}rio}{6}{section.1.3}% 
\contentsline {chapter}{\numberline {2}Estado da arte}{7}{chapter.2}% 
\contentsline {section}{\numberline {2.1}Trabalhos relacionados}{7}{section.2.1}% 
\contentsline {subsection}{\numberline {2.1.1}Sum\IeC {\'a}rio}{8}{subsection.2.1.1}% 
\contentsline {subsection}{\numberline {2.1.2}Contextualiza\IeC {\c c}\IeC {\~a}o de Solu\IeC {\c c}\IeC {\~o}es}{9}{subsection.2.1.2}% 
\contentsline {subsubsection}{Procura e Partilha de Diversos Tipos de Experi\IeC {\^e}ncias Internacionais}{9}{subsection.2.1.2}% 
\contentsline {subsubsection}{Procura e Partilha de Est\IeC {\'a}gios Internacionais}{9}{subsection.2.1.2}% 
\contentsline {subsubsection}{Procura e Partilha de Est\IeC {\'a}gios Internacionais em Parceria com Universidades}{10}{subsection.2.1.2}% 
\contentsline {subsubsection}{Procura e Partilha de Est\IeC {\'a}gios Internacionais de Acesso Exclusivo ao DEI ISEP}{10}{subsection.2.1.2}% 
\contentsline {subsection}{\numberline {2.1.3}Par\IeC {\^a}metros de Avalia\IeC {\c c}\IeC {\~a}o}{10}{subsection.2.1.3}% 
\contentsline {subsubsection}{Acesso a Pesquisa de Est\IeC {\'a}gios}{10}{subsection.2.1.3}% 
\contentsline {subsubsection}{Filtros de Pesquisa de Propostas}{11}{table.caption.8}% 
\contentsline {subsubsection}{Presen\IeC {\c c}a nas Redes Sociais}{12}{table.caption.9}% 
\contentsline {subsubsection}{Acesso \IeC {\`a} Cria\IeC {\c c}\IeC {\~a}o de Propostas}{13}{table.caption.10}% 
\contentsline {subsection}{\numberline {2.1.4}Considera\IeC {\c c}\IeC {\~o}es Gerais}{14}{subsection.2.1.4}% 
\contentsline {section}{\numberline {2.2}Tecnologias existentes}{15}{section.2.2}% 
\contentsline {subsection}{\numberline {2.2.1}Tecnologias Alternativas}{15}{subsection.2.2.1}% 
\contentsline {subsubsection}{\textit {Frameworks} Alternativas}{15}{subsection.2.2.1}% 
\contentsline {paragraph}{Teste 1}{15}{subsection.2.2.1}% 
\contentsline {paragraph}{Teste 2}{16}{table.caption.12}% 
\contentsline {paragraph}{Teste 3}{16}{table.caption.13}% 
\contentsline {subsubsection}{Considera\IeC {\c c}\IeC {\~o}es Gerais}{17}{table.caption.14}% 
\contentsline {paragraph}{Acessibilidade}{17}{table.caption.14}% 
\contentsline {paragraph}{Popularidade}{18}{figure.caption.15}% 
\contentsline {subsubsection}{Conclus\IeC {\~a}o}{19}{figure.caption.16}% 
\contentsline {subsection}{\numberline {2.2.2}Tecnologias Em Uso}{20}{subsection.2.2.2}% 
\contentsline {subsubsection}{PHP 5.6}{20}{subsection.2.2.2}% 
\contentsline {subsubsection}{Symfony 2.3}{20}{subsection.2.2.2}% 
\contentsline {subsubsection}{Twig}{20}{subsection.2.2.2}% 
\contentsline {subsubsection}{MySQL}{20}{subsection.2.2.2}% 
\contentsline {subsection}{\numberline {2.2.3}Vers\IeC {\~o}es Alternativas}{20}{subsection.2.2.3}% 
\contentsline {subsubsection}{PHP}{21}{subsection.2.2.3}% 
\contentsline {subsubsection}{Symfony}{21}{table.caption.17}% 
\contentsline {subsubsection}{Twig}{22}{table.caption.18}% 
\contentsline {subsubsection}{Conclus\IeC {\~a}o}{23}{table.caption.19}% 
\contentsline {chapter}{\numberline {3}An\IeC {\'a}lise e desenho da solu\IeC {\c c}\IeC {\~a}o}{25}{chapter.3}% 
\contentsline {section}{\numberline {3.1}Dom\IeC {\'\i }nio do problema}{25}{section.3.1}% 
\contentsline {section}{\numberline {3.2}Requisitos funcionais e n\IeC {\~a}o funcionais}{28}{section.3.2}% 
\contentsline {subsection}{\numberline {3.2.1}Requisitos}{28}{subsection.3.2.1}% 
\contentsline {subsection}{\numberline {3.2.2}Modelo FURPS+}{29}{subsection.3.2.2}% 
\contentsline {subsubsection}{Caracter\IeC {\'\i }sticas de Qualidade}{30}{subsection.3.2.2}% 
\contentsline {paragraph}{Funcionalidade}{30}{subsection.3.2.2}% 
\contentsline {paragraph}{Usabilidade}{30}{subsection.3.2.2}% 
\contentsline {paragraph}{Fiabilidade/Confiabilidade}{31}{subsection.3.2.2}% 
\contentsline {paragraph}{Desempenho}{31}{subsection.3.2.2}% 
\contentsline {paragraph}{Suportabilidade}{31}{subsection.3.2.2}% 
\contentsline {paragraph}{Restri\IeC {\c c}\IeC {\~o}es de \textit {Design}}{31}{subsection.3.2.2}% 
\contentsline {paragraph}{Restri\IeC {\c c}\IeC {\~o}es de Implementa\IeC {\c c}\IeC {\~a}o}{31}{subsection.3.2.2}% 
\contentsline {paragraph}{Restri\IeC {\c c}\IeC {\~o}es de \textit {Interface}}{31}{subsection.3.2.2}% 
\contentsline {subsection}{\numberline {3.2.3}Casos de Uso}{32}{subsection.3.2.3}% 
\contentsline {subsection}{\numberline {3.2.4}Engenharia de Requisitos}{34}{subsection.3.2.4}% 
\contentsline {subsubsection}{\textit {UC01 - Create Proposal}}{34}{subsection.3.2.4}% 
\contentsline {subsubsection}{\textit {UC04 - Evaluate Institution}}{36}{Item.6}% 
\contentsline {subsubsection}{\textit {UC06 - Update Proposal}}{38}{Item.12}% 
\contentsline {subsubsection}{\textit {UC14 - Purge Database}}{40}{Item.18}% 
\contentsline {subsubsection}{\textit {UC15 - Clone Proposal}}{42}{Item.24}% 
\contentsline {subsubsection}{\textit {UC18 - Search Proposal}}{44}{Item.30}% 
\contentsline {section}{\numberline {3.3}Desenho}{45}{section.3.3}% 
\contentsline {subsection}{\numberline {3.3.1}Estilo Arquitetural}{45}{subsection.3.3.1}% 
\contentsline {subsection}{\numberline {3.3.2}Intera\IeC {\c c}\IeC {\~a}o Gen\IeC {\'e}rica Utilizador-Sistema}{48}{subsection.3.3.2}% 
\contentsline {subsection}{\numberline {3.3.3}\textit {Use Case 01 - Create Proposal}}{50}{subsection.3.3.3}% 
\contentsline {subsubsection}{\textit {Modelo de Dom\IeC {\'\i }nio}}{50}{subsection.3.3.3}% 
\contentsline {subsubsection}{\textit {Diagrama de Classes}}{51}{figure.caption.34}% 
\contentsline {subsubsection}{\textit {Diagrama de Sequ\IeC {\^e}ncia}}{52}{figure.caption.35}% 
\contentsline {subsection}{\numberline {3.3.4}\textit {Use Case 04 - Evaluate Institution}}{53}{subsection.3.3.4}% 
\contentsline {subsubsection}{\textit {Modelo de Dom\IeC {\'\i }nio}}{53}{subsection.3.3.4}% 
\contentsline {subsubsection}{\textit {Diagrama de Classes}}{54}{figure.caption.37}% 
\contentsline {subsubsection}{\textit {Diagrama de Sequ\IeC {\^e}ncia}}{55}{figure.caption.38}% 
\contentsline {subsection}{\numberline {3.3.5}\textit {Use Case 06 - Update Proposal}}{56}{subsection.3.3.5}% 
\contentsline {subsubsection}{\textit {Modelo de Dom\IeC {\'\i }nio}}{56}{subsection.3.3.5}% 
\contentsline {subsubsection}{\textit {Diagrama de Classes}}{57}{figure.caption.40}% 
\contentsline {subsubsection}{\textit {Diagrama de Sequ\IeC {\^e}ncia}}{58}{figure.caption.41}% 
\contentsline {subsection}{\numberline {3.3.6}\textit {Use Case 14 - Purge Database}}{59}{subsection.3.3.6}% 
\contentsline {subsubsection}{\textit {Modelo de Dom\IeC {\'\i }nio}}{59}{subsection.3.3.6}% 
\contentsline {subsubsection}{\textit {Diagrama de Classes}}{59}{figure.caption.43}% 
\contentsline {subsubsection}{\textit {Diagrama de Sequ\IeC {\^e}ncia}}{60}{figure.caption.44}% 
\contentsline {subsection}{\numberline {3.3.7}\textit {Use Case 15 - Clone Proposal}}{61}{subsection.3.3.7}% 
\contentsline {subsubsection}{\textit {Modelo de Dom\IeC {\'\i }nio}}{61}{subsection.3.3.7}% 
\contentsline {subsubsection}{\textit {Diagrama de Classes}}{62}{figure.caption.46}% 
\contentsline {subsubsection}{\textit {Diagrama de Sequ\IeC {\^e}ncia}}{63}{figure.caption.47}% 
\contentsline {subsection}{\numberline {3.3.8}\textit {Use Case 18 - Search Proposal}}{64}{subsection.3.3.8}% 
\contentsline {subsubsection}{\textit {Modelo de Dom\IeC {\'\i }nio}}{64}{subsection.3.3.8}% 
\contentsline {subsubsection}{\textit {Diagrama de Classes}}{65}{figure.caption.49}% 
\contentsline {subsubsection}{\textit {Diagrama de Sequ\IeC {\^e}ncia}}{66}{figure.caption.50}% 
\contentsline {subsection}{\numberline {3.3.9}API de registo de utilizadores}{67}{subsection.3.3.9}% 
\contentsline {chapter}{\numberline {4}Implementa\IeC {\c c}\IeC {\~a}o da Solu\IeC {\c c}\IeC {\~a}o}{69}{chapter.4}% 
\contentsline {section}{\numberline {4.1}Descri\IeC {\c c}\IeC {\~a}o da implementa\IeC {\c c}\IeC {\~a}o}{69}{section.4.1}% 
\contentsline {subsection}{\numberline {4.1.1}\textit {Entity}}{69}{subsection.4.1.1}% 
\contentsline {subsubsection}{\textit {Many-To-Many}}{70}{subsection.4.1.1}% 
\contentsline {subsubsection}{\textit {One-To-Many \& Many-To-One}}{71}{lstnumber.4.2.7}% 
\contentsline {subsection}{\numberline {4.1.2}\textit {Controller}}{73}{subsection.4.1.2}% 
\contentsline {subsection}{\numberline {4.1.3}\textit {Service}}{78}{subsection.4.1.3}% 
\contentsline {subsection}{\numberline {4.1.4}\textit {Form}}{80}{subsection.4.1.4}% 
\contentsline {subsection}{\numberline {4.1.5}\textit {Views}}{81}{subsection.4.1.5}% 
\contentsline {section}{\numberline {4.2}Testes}{84}{section.4.2}% 
\contentsline {section}{\numberline {4.3}Avalia\IeC {\c c}\IeC {\~a}o da solu\IeC {\c c}\IeC {\~a}o}{85}{section.4.3}% 
\contentsline {chapter}{\numberline {5}Conclus\IeC {\~o}es}{87}{chapter.5}% 
\contentsline {section}{\numberline {5.1}Objetivos concretizados}{87}{section.5.1}% 
\contentsline {section}{\numberline {5.2}Limita\IeC {\c c}\IeC {\~o}es e trabalho futuro}{88}{section.5.2}% 
\contentsline {section}{\numberline {5.3}Aprecia\IeC {\c c}\IeC {\~a}o final}{90}{section.5.3}% 
\contentsline {chapter}{Refer\IeC {\^e}ncias}{91}{section.5.3}% 
\contentsline {chapter}{Anexos}{}{}
\cftpagenumberson {chapter}
\contentsline {chapter}{\numberline {A}Documenta\IeC {\c c}\IeC {\~a}o Complementar}{95}{appendix.A}% 
\contentsline {chapter}{\numberline {B}Excertos de C\IeC {\'o}digo}{101}{appendix.B}% 
